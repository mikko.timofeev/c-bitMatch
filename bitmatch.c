#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LENGTH 1000

const char *getSymbolBytecode (char c);
const char *getDecimalBytecode (int dec);
const char *itobase10 (char *buf, int value);

int main (int argc, char **argv)
{
	unsigned int	i									= 0;
	long			numberInteresting					= 0;	// long number of interesting bits / argv [2] converted
	char 			binary		[LENGTH] 				= "",	// binary form of a hex pattern (argv [1])
		 			buffer		[LENGTH] 				= "",
					*ptr1, *ptr2,
	
	// the most important variables follow
					the_binaryString		[LENGTH]	= "",	// sequence as a string / stdin converted
		 			the_binaryInteresting	[LENGTH]	= "";	// the pattern we care about / argv [1] converted
	
	if (argc != 3) // end if we have a wrong number of arguments (not 3)
	{
		return 1;
	}
	
	fread (buffer, 1, sizeof (buffer), stdin); // store piped output to "buffer"
	
	while (i < strlen (buffer)) // translate HEX to BIN symbol by symbol
	{
		strcat (the_binaryString, getSymbolBytecode (buffer [i++])); // append the next translated symbol
	}
	
	strcpy (binary, getDecimalBytecode (strtoul (argv [1], &ptr1, 16))); // convert binary representation of pattern to string

	numberInteresting = strtol (argv [2], &ptr2, 10); // get base 10 number of interesting bits
	
	strncpy (the_binaryInteresting, binary, numberInteresting); // make a requested pattern
	
	if (strstr (the_binaryString, the_binaryInteresting) == NULL) // check if there are matching bits
	{
		return 1;
	}
	return 0;
}

const char *getSymbolBytecode (char c) // convert symbolic string to byte code string
{
    char *byte = (char *) malloc (1); // allocate memory for one byte
	for
	(
		int i = 7, j = 0;
		i >= 0;
		--i, ++j
	)
    {
        byte [j] = ((c & (1 << i)) ? '1' : '0' ); // assign 0 / 1 to byte position
    }
	byte [8] = '\0'; // finish string
	return byte; // return byte of a symbol
}

const char *getDecimalBytecode (int n) // convert decimal number to byte code string
{
    char *binary = (char *) malloc (1);
	
	// array to store binary number
    int binaryNum [LENGTH];
 
    // counter for binary array
    int i = 0;
    while (n > 0)
	{
        // storing remainder in binary array
        binaryNum [i] = n % 2;
        n = n / 2;
        i++;
    }
	
	char int_str [15]; // be careful with the length of the buffer
    
	// printing binary array in reverse order
    for
	(
		int j = i - 1;
		j >= 0;
		j--
	)
	{
		strcat (binary, itobase10 (int_str, binaryNum [j]));
	}
	return binary;
}

const char *itobase10 (char *buf, int value) // convert the number to the string
{
    sprintf (buf, "%d", value);
    return buf;
}